#Predefine vars:
#${3RDPARTY_${lib}_SOURCE_DIR}
#${3RDPARTY_${lib}_BINARY_DIR}

#Define module vars
macro(_3rdparty_define_module_vars _lib)
  string(TOLOWER "${_lib}" lib)
  set(DUP_REPO "dup_repo")
  set(DUP_REPO_BIN "dup_repo_bin")
  set(IMP_SUFFIX _imp)
  set(${lib}_REPO_SOURCE_DIR "${3RDPARTY_${lib}_BINARY_DIR}/${DUP_REPO}")
  set(${lib}_REPO_BINARY_DIR "${3RDPARTY_${lib}_BINARY_DIR}/${DUP_REPO_BIN}")
  set(${lib}_SRC_DIR "${3RDPARTY_${lib}_SOURCE_DIR}/src_${lib}")
  option(3RDPARTY_BUILD_${lib} "Check if build ${lib}" ON)

  set(${lib}_ARDIR "archive")
  set(${lib}_LIBDIR "library")
  set(${lib}_RTDIR "runtime")
  set(${lib}_FWDIR "framework")
  set(${lib}_BDDIR "bundle")
  set(${lib}_INCDIR "include")

  if (BUILD_SHARED_LIBS)
    if (WIN32)
      set(${lib}_ARDIR "lib")
      set(${lib}_LIBDIR "lib")
      set(${lib}_RTDIR "bin")
      set(${lib}_INCDIR "inc")
    elseif(UNIX)
      set(${lib}_ARDIR "lib")
      set(${lib}_LIBDIR "lib")
      set(${lib}_RTDIR "bin")
      set(${lib}_INCDIR "include")
    endif()
  else()
    if (WIN32)
      set(${lib}_ARDIR "lib")
      set(${lib}_LIBDIR "lib")
      set(${lib}_RTDIR "bin")
      set(${lib}_INCDIR "inc")
    elseif(UNIX)
      set(${lib}_ARDIR "lib")
      set(${lib}_LIBDIR "lib")
      set(${lib}_RTDIR "bin")
      set(${lib}_INCDIR "include")
    endif()
  endif()

  set(${lib}_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/${lib}")
  if (UNIX)
    set(${lib}_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}")
  endif()

  set(${lib}_INSTALL_ARDIR "${${lib}_INSTALL_DIR}/${${lib}_ARDIR}")
  set(${lib}_INSTALL_LIBDIR "${${lib}_INSTALL_DIR}/${${lib}_LIBDIR}")
  set(${lib}_INSTALL_RTDIR "${${lib}_INSTALL_DIR}/${${lib}_RTDIR}")
  set(${lib}_INSTALL_FWDIR "${${lib}_INSTALL_DIR}/${${lib}_FWDIR}")
  set(${lib}_INSTALL_BDDIR "${${lib}_INSTALL_DIR}/${${lib}_BDDIR}")
  set(${lib}_INSTALL_INCDIR "${${lib}_INSTALL_DIR}/${${lib}_INCDIR}")

endmacro()

#predefine vars:
#${GIT}

#clone git repository and checkout revision.
macro(_3rdparty_clone_repo_checkout _name _revision)
  string(TOLOWER "${_name}" name)
  set(3RDPARTY_BUILD_${name}_REVISION "${_revision}")
  #fetch all remote branch.
  #execute_process(COMMAND ${GIT} fetch origin refs/remotes/*:refs/remotes/*
  #  WORKING_DIRECTORY "${${name}_SRC_DIR}")

  #clone git repository to binary dir.
  if (EXISTS "${${name}_REPO_SOURCE_DIR}")
    file(REMOVE_RECURSE "${${name}_REPO_SOURCE_DIR}")
  endif()

  file(COPY "${${name}_SRC_DIR}"
    DESTINATION "${3RDPARTY_${name}_BINARY_DIR}")
  file(RENAME "${3RDPARTY_${name}_BINARY_DIR}/src_${name}"
    "${3RDPARTY_${name}_BINARY_DIR}/${DUP_REPO}")
  file(REMOVE "${${name}_REPO_SOURCE_DIR}/.git")
  #execute_process(COMMAND ${GIT} --git-dir=${${name}_SRC_DIR}/.git --work-tree=./ checkout ${3RDPARTY_BUILD_${name}_REVISION}
  #  WORKING_DIRECTORY "${${name}_REPO_SOURCE_DIR}")

  #execute_process(COMMAND ${GIT} clone "${${name}_SRC_DIR}" "${${name}_REPO_SOURCE_DIR}")

  ##fetch all remote branch in cloned repository.
  ##execute_process(COMMAND ${GIT} fetch origin refs/remotes/*:refs/remotes/*
  ##  WORKING_DIRECTORY "${${name}_REPO_SOURCE_DIR}")

  ##checkout revision
  #execute_process(COMMAND ${GIT} checkout "${3RDPARTY_BUILD_${name}_REVISION}"
  #  WORKING_DIRECTORY "${${name}_REPO_SOURCE_DIR}")

endmacro()

#inject modified file to repository source dir.
macro(_3rdparty_inject_file _src _dst)
  get_filename_component(dst_dir "${_dst}" PATH)
  get_filename_component(dst_name "${_dst}" NAME)
  get_filename_component(src_name "${_src}" NAME)
  file(REMOVE "${${name}_REPO_SOURCE_DIR}/${dst_dir}/${dst_name}")
  file(COPY "${3RDPARTY_${name}_SOURCE_DIR}/${_src}" 
    DESTINATION "${${name}_REPO_SOURCE_DIR}/${dst_dir}")
  file(RENAME "${${name}_REPO_SOURCE_DIR}/${dst_dir}/${src_name}"
    "${${name}_REPO_SOURCE_DIR}/${dst_dir}/${dst_name}")
endmacro()

#install target
macro(_3rdparty_install_target tgtname modname)
  if (NOT CMAKE_CONFIGURATION_TYPES)
    install(TARGETS ${tgtname}
      ARCHIVE
      DESTINATION "${${modname}_INSTALL_ARDIR}"
      LIBRARY
      DESTINATION "${${modname}_INSTALL_LIBDIR}"
      RUNTIME
      DESTINATION "${${modname}_INSTALL_RTDIR}"
      FRAMEWORK
      DESTINATION "${${modname}_INSTALL_FWDIR}"
      BUNDLE
      DESTINATION "${${modname}_INSTALL_BDDIR}")
  else()
    foreach(conf ${CMAKE_CONFIGURATION_TYPES})
      install(TARGETS ${tgtname}
        ARCHIVE
        DESTINATION "${${modname}_INSTALL_ARDIR}/${conf}"
        CONFIGURATIONS ${conf} OPTIONAL
        LIBRARY
        DESTINATION "${${modname}_INSTALL_LIBDIR}/${conf}"
        CONFIGURATIONS ${conf} OPTIONAL
        RUNTIME
        DESTINATION "${${modname}_INSTALL_RTDIR}/${conf}"
        CONFIGURATIONS ${conf} OPTIONAL
        FRAMEWORK
        DESTINATION "${${modname}_INSTALL_FWDIR}/${conf}"
        CONFIGURATIONS ${conf} OPTIONAL
        BUNDLE
        DESTINATION "${${modname}_INSTALL_BDDIR}/${conf}"
        CONFIGURATIONS ${conf} OPTIONAL)
    endforeach()
  endif()
endmacro()

#install include directory
macro(_3rdparty_install_inc_dir dirname)
  install(DIRECTORY "${dirname}"
    DESTINATION "${${name}_INSTALL_INCDIR}/")
endmacro()

#install include files
macro(_3rdparty_install_inc_files)
  install(FILES ${ARGV}
    DESTINATION "${${name}_INSTALL_INCDIR}/")
endmacro()

#uninstall
macro(_3rdparty_uninstall _name)
	set(name ${_name})
	set(BINARY_DIR ${3RDPARTY_${name}_BINARY_DIR})
	set(INSTALL_DIR ${${name}_INSTALL_INCDIR})
	configure_file(
		  "${3RDPARTY_${name}_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
			"${3RDPARTY_${name}_BINARY_DIR}/cmake_uninstall.cmake"
			IMMEDIATE @ONLY)

	add_custom_target(uninstall
			COMMAND ${CMAKE_COMMAND} -P ${3RDPARTY_${name}_BINARY_DIR}/cmake_uninstall.cmake)
endmacro()
